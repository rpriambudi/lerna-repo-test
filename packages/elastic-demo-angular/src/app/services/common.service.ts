import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Menu, menuMapper } from './../menu';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private menuSubject: Subject<Menu[]> = new Subject<Menu[]>();

  constructor() { }

  public getMenuData(): void {
    this.menuSubject.next(menuMapper);
  }

  public getMenuObs(): Observable<Menu[]> {
    return this.menuSubject.asObservable();
  }

  public getMenuSubject(): Subject<Menu[]> {
    return this.menuSubject;
  }
}
