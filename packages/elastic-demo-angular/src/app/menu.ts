export class Menu {
    public name: string;
    public path: string;

    constructor(name: string, path: string) {
        this.name = name;
        this.path = path;
    }
}

export const menuMapper = [
    new Menu('Create FAQ', 'faq/create'),
    new Menu('Search FAQ', 'faq/search')
]