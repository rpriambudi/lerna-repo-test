import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { HttpService } from './interfaces/http-service.interface';

@Injectable()
export class HttpServiceImpl implements HttpService {
  constructor(private http: HttpClient) { }

  get<T>(url: string, query?: Map<string, any>): Observable<T> {
    if (query != null) {
      url = this.parseQueryString(url, query);
    }

    return this.http.get<T>(url, {
      observe: 'body',
      responseType: 'json'
    });
  }

  post<T>(url: string, body: any): Observable<T> {
    return this.http.post<T>(url, body, {
      observe: 'body',
      responseType: 'json'
    });
  }

  delete<T>(url: string): Observable<T> {
    return this.http.delete<T>(url, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'body',
      responseType: 'json'
    });
  }

  private parseQueryString(url: string, query: Map<string, any>) {
    url += '?';
    query.forEach((value: string, key: any) => {
      if (value)
        url += `${key}=${value}&`;
    });

    return url;
  }
}
