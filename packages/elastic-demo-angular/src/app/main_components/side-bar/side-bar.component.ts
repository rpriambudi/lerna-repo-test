import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Menu } from './../../menu';
import { CommonService } from './../../services/common.service';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  public menu: Menu[];

  constructor(private commonService: CommonService, private router: Router) {
  }

  ngOnInit(): void {
    this.commonService.getMenuObs()
    .subscribe(next => {
      this.menu = next;
    });
    this.commonService.getMenuData();
  }

  goTo(route: string) {
    this.router.navigateByUrl(route);
  }
}
