import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { FaqMainComponent } from './components/faq-main/faq-main.component';
import { FaqSearchComponent } from './components/faq-search/faq-search.component';
import { FaqServiceImpl } from './services/faq-service.impl';
import { FaqRoutingModule } from './faq-routing.module';
import { SharedModule } from './../shared/shared.module';
import { FaqCreateComponent } from './components/faq-create/faq-create.component';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [
    FaqMainComponent,
    FaqSearchComponent,
    FaqCreateComponent,
  ],
  imports: [
    FaqRoutingModule,
    HttpClientModule,
    CommonModule,
    SharedModule,
    MatDialogModule,
    MatExpansionModule,
    FormsModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatSelectModule
  ],
  providers: [
    {
      provide: 'FaqService',
      useClass: FaqServiceImpl
    }
  ]
})
export class FaqModule { }
