export class Faq {
    order: number;
    categoryId: number;
    categoryName: string;
    question: string;
    answer: string;
}

export class FaqAggregate {
    docCount: number;
    categoryName: string;
}

export class FaqSearchResult {
    documents: Faq[];
    aggregates: FaqAggregate[];
}