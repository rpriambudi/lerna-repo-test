import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Faq } from '../../models/faq.model';
import { FaqService } from './../../services/interfaces/faq-service.interface';
import { SimpleDialogComponent } from './../../../shared/components/simple-dialog/simple-dialog.component';

@Component({
  selector: 'app-faq-create',
  templateUrl: './faq-create.component.html',
  styleUrls: ['./faq-create.component.scss']
})
export class FaqCreateComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  faq: Faq = new Faq();
  category: any[] = [
    {
      value: 1,
      text: 'General'
    },
    {
      value: 2,
      text: 'Flora and Fauna'
    }
  ]
  private doneSubject = new Subject<void>();
  constructor(
    @Inject('FaqService') private faqService: FaqService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.doneSubject.next();
    this.doneSubject.complete();
  }

  onSubmit():void {
    this.isLoading = true;
    this.faq.order = parseInt(`${this.faq.order}`);
    this.faqService.create(this.faq)
    .pipe(
      takeUntil(this.doneSubject)
    )
    .subscribe(result => this.handleSuccess('Entry successfully created'), error => this.handleError(error));
  }

  private handleSuccess(message: string) {
    this.isLoading = false;
    const dialogRef = this.dialog.open(SimpleDialogComponent, {
      width: '300px',
      data: { message: message}
    });

    dialogRef.afterClosed().subscribe(() => {
      this.faq = new Faq();
    });
  }

  private handleError(error) {
    this.isLoading = false;
    this.dialog.open(SimpleDialogComponent, {
      width: '300px',
      data: { message: error}
    });
  }
}
