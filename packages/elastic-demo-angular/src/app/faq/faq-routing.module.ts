import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { FaqMainComponent } from './components/faq-main/faq-main.component';
import { FaqSearchComponent } from './components/faq-search/faq-search.component';
import { FaqCreateComponent } from './components/faq-create/faq-create.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full'
    },
    {
        path: 'main',
        component: FaqMainComponent,
    },
    {
        path: 'create',
        component: FaqCreateComponent,
    },
    {
        path: 'search',
        component: FaqSearchComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FaqRoutingModule {}