import { Faq, FaqSearchResult } from './../../models/faq.model';
import { Observable } from 'rxjs';

export interface FaqService {
    create(faq: Faq): Observable<any>;
    update(faqId: number, faq: Faq): Observable<any>;
    find(): Observable<Faq[]>;
    findById(faqId: number): Observable<Faq>;
    delete(faqId: number): Observable<any>;
    search(query?: string): Observable<FaqSearchResult>;
}