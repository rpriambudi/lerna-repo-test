import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HealthcheckComponent } from './main_components/healthcheck/healthcheck.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'faq',
    pathMatch: 'full'
  },
  {
    path: 'healthcheck',
    component: HealthcheckComponent
  },
  {
    path: 'faq',
    loadChildren: () =>  import('./faq/faq.module').then(m => m.FaqModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
