# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.9](https://bitbucket.org/rpriambudi/lerna-repo-test/compare/v1.0.8...v1.0.9) (2020-07-03)

**Note:** Version bump only for package rpriambudi-elastic-demo-angular





## [1.0.8](https://bitbucket.org/rpriambudi/lerna-repo-test/compare/v1.0.7...v1.0.8) (2020-07-03)

**Note:** Version bump only for package rpriambudi-elastic-demo-angular





## [1.0.7](https://bitbucket.org/rpriambudi/lerna-repo-test/compare/v1.0.6...v1.0.7) (2020-07-03)

**Note:** Version bump only for package rpriambudi-elastic-demo-angular





## [1.0.6](https://bitbucket.org/rpriambudi/lerna-repo-test/compare/v1.0.5...v1.0.6) (2020-07-03)

**Note:** Version bump only for package rpriambudi-elastic-demo-angular





## [1.0.5](https://bitbucket.org/rpriambudi/lerna-repo-test/compare/v1.0.4...v1.0.5) (2020-07-03)

**Note:** Version bump only for package rpriambudi-elastic-demo-angular
